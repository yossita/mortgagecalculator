using System;
using MortgageCalculator.API.Model;
using Xunit;

namespace MortgageCalculator.Tests
{
    public class FixedRateMortgateCalculatorServiceTests
    {
        
        [Theory]
        [InlineData(200000d, 30, 0.065d, "Monthly", 1264.14d)]
        [InlineData(2000000d, 30, 0.065d, "Monthly", 12641.36d)]
        [InlineData(20000d, 30, 0.065d, "Monthly", 126.41d)]
        public void CanCalculateRecurringPayment(double amount, int amortizationPeriod, double yearlyInterestRate, string schedule, double expectedRecurringPayment)
        {
            var svc = new FixedRateMortgateCalculatorService();
            var result = svc.CalculateRecurringPayment(amount, (MotrtgageSchedule)Enum.Parse(typeof(MotrtgageSchedule), schedule), amortizationPeriod, yearlyInterestRate);

            Assert.Equal(new Money(expectedRecurringPayment), result);
        }


        [Theory]
        [InlineData(1264.14d, 30, 0.065d, "Monthly", 200000.63d)]
        [InlineData(12641.36d, 30, 0.065d, "Monthly", 1999999.93d)]
        [InlineData(126.41d, 30, 0.065d, "Monthly", 19999.43d)]
        public void CanCalculatePrincipal(double recurringPayment, int amortizationPeriod, double yearlyInterestRate, string schedule, double expectedPrincipal)
        {          
            var svc = new FixedRateMortgateCalculatorService();
            var result = svc.CalculatePrincipal(recurringPayment, (MotrtgageSchedule)Enum.Parse(typeof(MotrtgageSchedule), schedule), amortizationPeriod, yearlyInterestRate);

            Assert.Equal(new Money(expectedPrincipal), result);
        }

    }
}
