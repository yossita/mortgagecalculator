using System;
using MortgageCalculator.API.Model;
using Xunit;

namespace MortgageCalculator.Tests
{
    public class MortgageInsuranceCalculatorServiceTests
    {
        [Theory]
        [InlineData(20000, 0.05, 0.0315)]
        [InlineData(20000, 0.0999, 0.0315)]
        [InlineData(20000, 0.1, 0.024)]
        [InlineData(20000, 0.1499, 0.024)]
        [InlineData(20000, 0.15, 0.018)]
        [InlineData(20000, 0.1999, 0.018)]
        [InlineData(20000, 0.2, 0)]
        public void CreateFromTotalAmount_InsuranceCostCalculated(double totalAmount, double downPaymentPercent, double expectedCostPercent)
        {
            var svc = new MortgageInsuranceCostCalculatorService();
            var cost = svc.CalculateCost(totalAmount, totalAmount * downPaymentPercent);
            Assert.Equal(expectedCostPercent, cost / totalAmount, 4);
        }

    }
}