using System;
using MortgageCalculator.API.Model;
using Xunit;

namespace MortgageCalculator.Tests
{
    public class MortgageTests
    {
        private readonly IMortgageInsuranceCostCalculatorService _insuranceCostCalculator;
        private readonly IMortgageCalculatorService _paymentCalculator;

        public MortgageTests()
        {
            _insuranceCostCalculator = new MortgageInsuranceCostCalculatorService();
            _paymentCalculator = new FixedRateMortgateCalculatorService();
        }

        [Fact]
        public void CreateFromPrincipal_DownPaymentTooSmall_Throws()
        {

            var principal = 50000;
            var downPayment = 100;
            var amortizationPeriod = 1;
            var schedule = MotrtgageSchedule.Weekly;
            var rate = 0.05;

            Assert.Throws<ApplicationException>(() => Mortgage.CreateFromTotalAmount(principal, downPayment, amortizationPeriod, schedule, rate, _paymentCalculator, _insuranceCostCalculator));
        }

        [Fact]
        public void CreateFromPrincipal_MortgageWithRecurringPayment()
        {

            var amount = 50000;
            var downPayment = 2500;
            var amortizationPeriod = 10;
            var schedule = MotrtgageSchedule.Weekly;
            var rate = 0.05;

            var mortgage = Mortgage.CreateFromTotalAmount(amount, downPayment, amortizationPeriod, schedule, rate, _paymentCalculator, _insuranceCostCalculator);

            Assert.True(mortgage.RecurringPayment > 0);
        }

        [Fact]
        public void CreateFromRecurringPayment_MortgageWithPrincipal()
        {

            var payment = 40;
            var downPayment = 2000;
            var amortizationPeriod = 10;
            var schedule = MotrtgageSchedule.Weekly;
            var rate = 0.05;

            var mortgage = Mortgage.CreateFromRecurringPayment(payment, downPayment, amortizationPeriod, schedule, rate, _paymentCalculator, _insuranceCostCalculator);

            Assert.True(mortgage.Principal > 0);
        }

    }
}