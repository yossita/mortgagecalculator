using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using Microsoft.AspNetCore.Mvc;
using MortgageCalculator.API.Controllers;
using MortgageCalculator.API.Model;
using Xunit;

namespace MortgageCalculator.Tests
{

    public class MortgageCalculatorControllerTests
    {

        [Fact]
        public void CanChangeYearlyInterestRate()
        {
            var rate = 0.05;
            var newRate = 0.07;
            var rateProvider = new InterestRateProvider();
            rateProvider.Set(RateTerm.Yearly, rate);

            var controller = new MortgageCalculatorController(new FixedRateMortgateCalculatorService(), rateProvider, new MortgageInsuranceCostCalculatorService());

            var result = controller.ChangeYearlyInterestRate(newRate);

            var response = Assert.IsType<OkObjectResult>(result.Result);
            var value = response.Value.ToDynamic();
            Assert.Equal<double>(rate, value.oldRate);
            Assert.Equal<double>(newRate, value.newRate);
        }

        [Theory]
        [InlineData(500, 100, 5, "weekly")]
        public void CalculatePaymentAmount_Ok(double amount, double downPayment, int amortizationPeriod, string schedule)
        {
            var controller = new MortgageCalculatorController(new FixedRateMortgateCalculatorService(), new InterestRateProvider(), new MortgageInsuranceCostCalculatorService());

            var result = controller.CalculatePaymentAmount(amount, downPayment, schedule, amortizationPeriod);

            var response = Assert.IsType<OkObjectResult>(result.Result);
            var value = response.Value.ToDynamic();
            Assert.True(value.paymentAmount > 0);
        }

        [Theory]
        [InlineData(500, 10, 5, "weekly")]
        [InlineData(500, 100, 4, "weekly")]
        [InlineData(1200000, 100000, 5, "weekly")]
        public void CalculatePaymentAmount_BadRequest(double amount, double downPayment, int amortizationPeriod, string schedule)
        {
            var controller = new MortgageCalculatorController(new FixedRateMortgateCalculatorService(), new InterestRateProvider(), new MortgageInsuranceCostCalculatorService());

            var result = controller.CalculatePaymentAmount(amount, downPayment, schedule, amortizationPeriod);

            var response = Assert.IsType<BadRequestObjectResult>(result.Result);
            var value = response.Value.ToDynamic();
            Assert.NotNull(value.error);
        }

        [Theory]
        [InlineData(10, 1000, 5, "weekly")]
        [InlineData(10, 1000, 25, "weekly")]
        [InlineData(1200, 100000, 25, "monthly")]
        public void CalculateMortgageAmount_Ok(double payment, double downPayment, int amortizationPeriod, string schedule)
        {
            var controller = new MortgageCalculatorController(new FixedRateMortgateCalculatorService(), new InterestRateProvider(), new MortgageInsuranceCostCalculatorService());

            var result = controller.CalculateMortgageAmount(payment, downPayment, schedule, amortizationPeriod);

            var response = Assert.IsType<OkObjectResult>(result.Result);
            var value = response.Value.ToDynamic();
            Assert.True(value.principalAmount > 0);
        }

        [Theory]
        [InlineData(10, 10, 5, "weekly")]
        public void CalculateMortgageAmount_BadRequest(double payment, double downPayment, int amortizationPeriod, string schedule)
        {
            var controller = new MortgageCalculatorController(new FixedRateMortgateCalculatorService(), new InterestRateProvider(), new MortgageInsuranceCostCalculatorService());

            var result = controller.CalculateMortgageAmount(payment, downPayment, schedule, amortizationPeriod);

            var response = Assert.IsType<BadRequestObjectResult>(result.Result);
            var value = response.Value.ToDynamic();
            Assert.NotNull(value.error);
        }

    }

    public static class DynamicExtensions
    {
        public static dynamic ToDynamic(this object value)
        {
            IDictionary<string, object> expando = new ExpandoObject();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
                expando.Add(property.Name, property.GetValue(value));

            return expando as ExpandoObject;
        }
    }
}