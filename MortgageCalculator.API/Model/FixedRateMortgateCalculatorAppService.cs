using System;
using static System.Math;

namespace MortgageCalculator.API.Model
{

    public interface IMortgageCalculatorService
    {
        Money CalculateRecurringPayment(Money principal, MotrtgageSchedule schedule, int amortizationPeriod, double yearlyInterestRate);
        Money CalculatePrincipal(Money paymentAmount, MotrtgageSchedule schedule, int amortizationPeriod, double yearlyInterestRate);
    }

    public class FixedRateMortgateCalculatorService : IMortgageCalculatorService
    {

        public Money CalculateRecurringPayment(Money principal, MotrtgageSchedule schedule, int amortizationPeriod, double yearlyInterestRate)
        {
            var (numberOfPayments, paymentInterestRate) = GetRecurringPaymentDetails(amortizationPeriod, yearlyInterestRate, schedule);

            var paymentAmount = principal * CalculateFixedRateCompoundRatio(numberOfPayments, paymentInterestRate);
            return paymentAmount;
        }

        public Money CalculatePrincipal(Money paymentAmount, MotrtgageSchedule schedule, int amortizationPeriod, double yearlyInterestRate)
        {
            var (numberOfPayments, paymentInterestRate) = GetRecurringPaymentDetails(amortizationPeriod, yearlyInterestRate, schedule);
            var principal = paymentAmount / CalculateFixedRateCompoundRatio(numberOfPayments, paymentInterestRate);
            return principal;
        }

        private static double CalculateFixedRateCompoundRatio(int numberOfPayments, double paymentInterestRate)
        {
            return (paymentInterestRate * (Pow((1.0 + paymentInterestRate), numberOfPayments)) / (Pow(1.0 + paymentInterestRate, numberOfPayments) - 1));
        }

        private static (int term, double interestRatePerTerm) GetRecurringPaymentDetails(int amortizationPeriod, double yearlyInterestRate, MotrtgageSchedule schedule)
        {
            //Can be refactored into individual policies/strategies
            int numberOfPaymentsPerYear;
            switch (schedule)
            {
                case MotrtgageSchedule.Weekly:
                    numberOfPaymentsPerYear = 52;
                    break;
                case MotrtgageSchedule.Biweekly:
                    numberOfPaymentsPerYear = 52 / 2;
                    break;
                case MotrtgageSchedule.Monthly:
                    numberOfPaymentsPerYear = 12;
                    break;
                default:
                    throw new ArgumentException($"Cannot calculate number of payment for undefined schedule", nameof(schedule));
            }
            double paymentInterestRate = yearlyInterestRate / numberOfPaymentsPerYear;
            int numberOfPayments = amortizationPeriod * numberOfPaymentsPerYear;

            return (term: numberOfPayments, interestRatePerTerm: paymentInterestRate);
        }
    }

}