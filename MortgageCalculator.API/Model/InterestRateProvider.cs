namespace MortgageCalculator.API.Model
{


    public interface IInterestRateProvider
    {
        void Set(RateTerm term, double rate);
        double Get(RateTerm term);
    }

    public enum RateTerm
    {
        Undefined,
        Yearly
    }

    public class InterestRateProvider : IInterestRateProvider
    {

        private double _rate = 0.025;

        public void Set(RateTerm term, double rate)
        {
            _rate = rate;
        }

        public double Get(RateTerm term)
        {
            return _rate;
        }

    }
}