using System;
using System.Collections.Generic;

namespace MortgageCalculator.API.Model
{

    public interface IMortgageValidator
    {
        (bool valid, string reason) IsValidFor(Mortgage mortgage);
    }

    public class AmortizationPriodRangeValidator : IMortgageValidator
    {
        public (bool valid, string reason) IsValidFor(Mortgage mortgage)
        {

            var valid = mortgage.AmortizationPeriod >= 5 && mortgage.AmortizationPeriod <= 25;
            return (valid, valid ? string.Empty : $"Amortization period must be between 5 and 25 years");
        }

    }

    public class DownPaymentSufficientValidator : IMortgageValidator
    {
        public (bool valid, string reason) IsValidFor(Mortgage mortgage)
        {
            var downPaymentRequired = CalculateRequiredDownPaymentForPrincipal(mortgage.TotalAmount);
            var valid = mortgage.DownPayment >= downPaymentRequired;
            return (valid, valid ? string.Empty : $"Down payment must be at least {downPaymentRequired} for a mortgage of {mortgage.TotalAmount}");
        }

        private static Money CalculateRequiredDownPaymentForPrincipal(Money principal)
        {
            if (principal <= 500000) return principal * 0.05;
            return (principal - 500000) * 0.10 + new Money(500000) * 0.05;
        }

    }

    public class InsuranceMaximumAmountValidator : IMortgageValidator
    {
        public (bool valid, string reason) IsValidFor(Mortgage mortgage)
        {
            var valid = !(mortgage.TotalAmount > 1000000d && mortgage.DownPayment / mortgage.TotalAmount < 0.2);
            return (valid, valid ? string.Empty : $"Insurance for mortgage above $1000000 and down payment less than 20% is not available");
        }

    }
    public class MortgageValidationFactory
    {

        public IEnumerable<IMortgageValidator> Create() => new IMortgageValidator[]{
                new DownPaymentSufficientValidator(),
                new AmortizationPriodRangeValidator(),
                new InsuranceMaximumAmountValidator()
            };
    }
}