using System;
using System.Collections.Generic;

namespace MortgageCalculator.API.Model
{

    public interface IMortgageInsuranceCostCalculatorService
    {
        Money CalculateCost(Money totalMortgageAmount, Money downPayment);
    }

    public class MortgageInsuranceCostCalculatorService : IMortgageInsuranceCostCalculatorService
    {

        private static Dictionary<double, double> _insuranceCosts = new Dictionary<double, double>(){
            {0.05, 0.0315},
            {0.1, 0.024},
            {0.15, 0.018},
            {0.2, 0}
        };

        public Money CalculateCost(Money totalMortgageAmount, Money downPayment)
        {
            var costRate = LookupCostRate(downPayment / totalMortgageAmount);
            return totalMortgageAmount * costRate;
        }

        private static double LookupCostRate(double downPaymentRatio)
        {
            var keys = new double[_insuranceCosts.Count];
            _insuranceCosts.Keys.CopyTo(keys, 0);
            int i = 1;
            for (; i < keys.Length; i++)
            {
                if (downPaymentRatio < keys[i]) return _insuranceCosts[keys[i - 1]];
            }
            return _insuranceCosts[keys[i - 1]];
        }
    }

}