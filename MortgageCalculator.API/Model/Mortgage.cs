using System;
using System.Collections.Generic;
using System.Linq;

namespace MortgageCalculator.API.Model
{

    public enum MotrtgageSchedule
    {
        Undefined,
        Weekly,
        Biweekly,
        Monthly
    }

    public class Mortgage
    {
        public Money Principal { get; private set; }
        public Money DownPayment { get; private set; }
        public int AmortizationPeriod { get; private set; }
        public MotrtgageSchedule Schedule { get; private set; }
        public double YearlyInterestRate { get; private set; }
        public Money RecurringPayment { get; private set; }
        public Money InsuranceCost { get; private set; }

        public Money TotalAmount => Principal + DownPayment + InsuranceCost;

        public static Mortgage CreateFromTotalAmount(Money amount, Money downPayment, int amortizationPeriod, MotrtgageSchedule schedule, double yearlyInterestRate,
            IMortgageCalculatorService mortgageCalculator, IMortgageInsuranceCostCalculatorService insuranceCostProvider)
        {
            var insuranceCost = insuranceCostProvider.CalculateCost(amount, downPayment);
            var principal = amount - downPayment - insuranceCost;
            var recurringPayment = mortgageCalculator.CalculateRecurringPayment(principal + insuranceCost, schedule, amortizationPeriod, yearlyInterestRate);

            var mortgage = new Mortgage()
            {
                Principal = principal,
                DownPayment = downPayment,
                AmortizationPeriod = amortizationPeriod,
                Schedule = schedule,
                YearlyInterestRate = yearlyInterestRate,
                InsuranceCost = insuranceCost,
                RecurringPayment = recurringPayment
            };


            var errors = Validate(mortgage, new MortgageValidationFactory().Create());
            if (errors.Any()) throw new ApplicationException(string.Join(", ", errors));

            return mortgage;
        }

        public static Mortgage CreateFromRecurringPayment(Money recurringPayment, Money downPayment, int amortizationPeriod, MotrtgageSchedule schedule, double yearlyInterestRate,
            IMortgageCalculatorService mortgageCalculator, IMortgageInsuranceCostCalculatorService insuranceCostCalculator)
        {
            var principal = mortgageCalculator.CalculatePrincipal(recurringPayment, schedule, amortizationPeriod, yearlyInterestRate);
            var insuranceCost = insuranceCostCalculator.CalculateCost(principal + downPayment, downPayment);

            var mortgage = new Mortgage()
            {
                Principal = principal,
                DownPayment = downPayment,
                AmortizationPeriod = amortizationPeriod,
                Schedule = schedule,
                YearlyInterestRate = yearlyInterestRate,
                InsuranceCost = insuranceCost,
                RecurringPayment = recurringPayment
            };

            var errors = Validate(mortgage, new MortgageValidationFactory().Create());
            if (errors.Any()) throw new ApplicationException(string.Join(", ", errors));

            return mortgage;
        }

        private static IEnumerable<string> Validate(Mortgage mortgage, IEnumerable<IMortgageValidator> validators)
        {
            foreach (var validator in validators)
            {
                var validationResult = validator.IsValidFor(mortgage);
                if (!validationResult.valid) yield return validationResult.reason;
            }
        }

    }

}
