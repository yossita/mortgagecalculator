using System;
using static System.Math;

namespace MortgageCalculator.API.Model
{
    public struct Money
    {
        private readonly double _value;
        private readonly int _precision;

        public Money(double value, int precision = 2)
        {
            if (precision < 0) throw new ArgumentException($"Money percision must be 0 or positive", nameof(precision));
            _value = Round(value, precision);
            _precision = precision;
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Money)) return false;
            var other = (Money)obj;
            return _value==other._value && _precision==other._precision;
        }

        public static Money operator +(Money one, Money another)
        {
            return new Money(one._value + another._value, one._precision);
        }

        public static Money operator -(Money one, Money another)
        {
            return new Money(one._value - another._value, one._precision);
        }

        public static Money operator *(Money one, double number)
        {
            return new Money(one._value * number, one._precision);
        }

        public static implicit operator Money(double amount){
            return new Money(amount);
        }

        public static implicit operator double(Money money){
            return money._value;
        }

    }

}