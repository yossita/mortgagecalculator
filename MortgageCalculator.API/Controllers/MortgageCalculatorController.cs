using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MortgageCalculator.API.Model;
using Newtonsoft.Json;

namespace MortgageCalculator.API.Controllers
{

    [ApiController]
    [Route("")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public partial class MortgageCalculatorController : ControllerBase
    {

        private readonly IMortgageCalculatorService _mortgageCalculator;
        private readonly IInterestRateProvider _interestRateProvider;
        private readonly IMortgageInsuranceCostCalculatorService _mortgageInsuranceCostService;

        public MortgageCalculatorController(
            IMortgageCalculatorService mortgageCalculator,
            IInterestRateProvider interestRateProvider,
            IMortgageInsuranceCostCalculatorService mortgageInsuranceCostService)
        {
            _mortgageCalculator = mortgageCalculator;
            _interestRateProvider = interestRateProvider;
            _mortgageInsuranceCostService = mortgageInsuranceCostService;
        }

        [HttpGet("payment-amount")]
        public ActionResult<dynamic> CalculatePaymentAmount(double mortgageAmount, double downPayment, string schedule, int amortizationPeriodInYears)
        {
            try
            {
                var mortgage = Mortgage.CreateFromTotalAmount(mortgageAmount,
                    downPayment,
                    amortizationPeriodInYears,
                    (MotrtgageSchedule)Enum.Parse(typeof(MotrtgageSchedule), schedule, true),
                    _interestRateProvider.Get(RateTerm.Yearly),
                    _mortgageCalculator,
                    _mortgageInsuranceCostService);
                return Ok(new { paymentAmount = mortgage.RecurringPayment.ToString() });
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpGet("mortgage-amount")]
        public ActionResult<dynamic> CalculateMortgageAmount(double paymentAmount, double? downPayment, string schedule, int amortizationPeriodInYears)
        {
            try
            {
                var mortgage = Mortgage.CreateFromRecurringPayment(paymentAmount,
                    downPayment ?? 0d,
                    amortizationPeriodInYears,
                    (MotrtgageSchedule)Enum.Parse(typeof(MotrtgageSchedule), schedule, true),
                    _interestRateProvider.Get(RateTerm.Yearly),
                    _mortgageCalculator,
                    _mortgageInsuranceCostService);

                return Ok(new { principalAmount = mortgage.TotalAmount.ToString() });
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpPatch("interest-rate")]
        public ActionResult<dynamic> ChangeYearlyInterestRate(double newRate)
        {
            try
            {
                var oldRate = _interestRateProvider.Get(RateTerm.Yearly);
                _interestRateProvider.Set(RateTerm.Yearly, newRate);

                return Ok(new { newRate = newRate.ToString(), oldRate = oldRate.ToString() });
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

    }
}